import { Subject } from 'rxjs';

import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  // Observable string sources
  private emitChangeSource = new Subject();

  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  // Service message commands
  emitChange(change: string) {
    this.emitChangeSource.next(change);
  }
}
